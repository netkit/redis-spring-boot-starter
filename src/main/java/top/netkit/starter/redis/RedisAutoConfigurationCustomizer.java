package top.netkit.starter.redis;

import org.springframework.boot.autoconfigure.data.redis.RedisProperties;

/**
 * redis auto configuration customizer
 * @author shixinke
 */
public interface RedisAutoConfigurationCustomizer {

    /**
     * customize
     * @param config config properties
     */
    void customize(final RedisProperties config);
}

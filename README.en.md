# redis-spring-boot-starter

#### Description
spring boot redis starter

#### Installation

1.  add this package to maven config

```xml
<dependency>
    <groupId>top.netkit.starter</groupId>
    <artifactId>redis-spring-boot-starter</artifactId>
    <version>2.5.2</version>
</dependency>
```    

2.  add redis configuration into the project configuration file

```
spring.redis.host=127.0.0.1
spring.redis.port=6379
spring.redis.database=0
spring.redis.password=password
```


3.  use the redis client

```java
package test;

import top.netkit.redis.client.executor.RedisClient;

import javax.annotation.Resource;

public class RedisDemo {

    @Resource
    private RedisClient redisClient;
    
    public void setTest() {
        redisClient.set("test_key", "test_value");
    }
}
```



